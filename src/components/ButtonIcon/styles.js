import { StyleSheet } from 'react-native';
import * as colors from 'app/config/colors';
import * as sizes from 'app/config/sizes';

export default props =>
  StyleSheet.create({
    btn: StyleSheet.flatten([
      {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
      
      },
      props.style && {
        ...props.style
      },
    ]),

  });

