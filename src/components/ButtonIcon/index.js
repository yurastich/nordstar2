import React, { Component } from 'react';
import {
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'app/components/Icon';

import styles from './styles';

export default class ButtonIcon extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    name: PropTypes.string,

    // styling
    style: PropTypes.object,
    iconStyle: PropTypes.object,
  }

  render() {
    const { props } = this;
    const style = styles(props);


    return (
      <TouchableOpacity
        onPress={this.onPress}
        style={style.btn}
        activeOpacity={.8}
      >

        <Icon 
          name={props.name}
          iconStyle={props.iconStyle}
        />

      </TouchableOpacity>
    )
  }

  onPress = () => {
    const { props } = this;

    if (props.onPress) {
      props.onPress()
    }
  }
}