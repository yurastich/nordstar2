import { StyleSheet } from 'react-native';
import * as sizes from 'app/config/sizes';
import * as colors from 'app/config/colors';

export default props =>
  StyleSheet.create({

    container: StyleSheet.flatten([
      {
        backgroundColor: colors.BLUE,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: props.statusBarHeight,
        paddingHorizontal: sizes.GUTTER,
        height: props.statusBarHeight + 44,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0
      },
      !props.leftContent &&
      !props.rightContent && {
        justifyContent: 'center',
      }

    ]),

    bg: StyleSheet.flatten([
      {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      }
    ]),

    logo: StyleSheet.flatten([
      {
        width: 130,
        height: 30,
        alignSelf: 'center',
      }
    ]),

    side: StyleSheet.flatten([
      {
        flexShrink: 0,
        flexGrow: 0,
      }
    ]),

    center: StyleSheet.flatten([
      {
        flexShrink: 1
      }
    ]),

    title: StyleSheet.flatten([
      {
        fontSize: sizes.FONT_SIZE - 2,
        color: '#fff',
        fontWeight: '700',
        paddingHorizontal: 5,
        textAlign: 'center'
      }
    ]),
  });

