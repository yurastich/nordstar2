import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    Text,
    Platform,
    Image,
    ImageBackground,
    TouchableOpacity,
    StatusBar
} from 'react-native';

import ButtonIcon from 'app/components/ButtonIcon';

import { Actions } from 'react-native-router-flux';
import * as scenes from "app/config/sceneKeys";


import styles from './styles';


export default class NavBar extends Component {

    bg = require('app/images/navbar.png');
    logo = require('app/images/logo_navbar.png');

    static propTypes = {
        title: PropTypes.string,
        leftContent: PropTypes.bool,
        rightContent: PropTypes.bool,
        canterContent: PropTypes.bool,
        style: PropTypes.object,
    }

    static defaultProps = {
        title: '',
        leftContent: true,
        rightContent: true,
        canterContent: false,
        statusBarHeight: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
    };

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <View
                style={style.container}
            >
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <ImageBackground
                    style={style.bg}
                    imageStyle={{ resizeMode: "stretch" }}
                    source={this.bg}
                />


                {props.leftContent &&
                    <View style={style.side}>
                        <ButtonIcon
                            name='bars'
                            onPress={this.actionLeft}
                        />
                    </View>
                }

                {props.centerContent &&
                    <View style={style.center}>
                        <Text
                            style={style.title}
                            numberOfLines={1}
                        >
                            {props.title}
                        </Text>
                    </View>
                }

                {props.rightContent &&
                    <View style={style.side}>
                        <TouchableOpacity
                            onPress={this.actionRight}
                            activeOpacity={.8}
                        >
                            <Image
                                style={style.logo}
                                source={this.logo}
                                resizeMode="stretch"
                            />
                        </TouchableOpacity>

                    </View>
                }

            </View>
        )
    }

    actionLeft = () => {
        const { props } = this;
        Actions.drawerOpen()

        if (props.actionLeft) {
            props.actionLeft()
        }
    }

    actionRight = () => {
        const { props } = this;
        const setScene = scenes.MAIN.key;

        if(setScene !== Actions.currentScene){
            Actions[setScene].call();
        }else{
            alert(`sorry, now you are on the ${setScene} scene`)
        }

        if (props.actionRight) {
            props.actionRight()
        }
    }
}