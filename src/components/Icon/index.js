import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconNative from 'react-native-vector-icons/FontAwesome';

export default class Icon extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    name: PropTypes.string.isRequired,

    // styling
    color: PropTypes.string,
    size: PropTypes.number,
    iconStyle: PropTypes.object
  }

  static defaultProps = {
    size: 20,
    color: '#fff'
  };

  render() {
    const { props } = this;
    const iconProps = {
      name: props.name,
      size: props.size,
      color: props.color,
      ...props.iconStyle
    }

    return (
      <IconNative {...iconProps} />
    )
  }
}