import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    ScrollView,
    View,
    Platform,
    StatusBar
} from 'react-native';

import styles from './styles';


class Content extends Component {

    static propTypes = {
        style: PropTypes.object,
    }

    static defaultProps = {
        statusBarHeight: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
    };

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <View style={style.container}>
                <ScrollView>
                    <View
                        style={style.grid}
                    >
                        {props.children}
                    </View>
                </ScrollView>
            </View>
        )
    }
}


export default Content;