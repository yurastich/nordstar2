import { StyleSheet } from 'react-native';
import * as sizes from 'app/config/sizes';
import * as colors from 'app/config/colors';


export default props =>
  StyleSheet.create({

    container: StyleSheet.flatten([
      {
        flex: 1,
        backgroundColor: colors.BLUE
      },

    ]),

    grid: StyleSheet.flatten([
      {
        paddingTop: props.statusBarHeight + 44,
        paddingHorizontal: sizes.GUTTER,
      },

    ]),

  });

