import { StyleSheet } from 'react-native';

export default props =>
  StyleSheet.create({

    container: StyleSheet.flatten([
      {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 2
      },
    ]),

    bg: StyleSheet.flatten([
      {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      }
    ])
  });

