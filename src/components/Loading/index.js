import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    Text,
    Platform,
    Image,
    ImageBackground,
    StatusBar
} from 'react-native';

import ButtonIcon from 'app/components/ButtonIcon';

import { Actions } from 'react-native-router-flux';

// import * as stylesConfig from 'app/config/style';
import styles from './styles';


export default class Loading extends Component {

    bg = require('app/images/loading.png');



    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <View
                style={style.container}
            >
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <ImageBackground
                    style={style.bg}
                    imageStyle={{ resizeMode: "stretch" }}
                    source={this.bg}
                />

            </View>
        )
    }

}