import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text
} from 'react-native';

import Icon from 'app/components/Icon';
import * as colors from 'app/config/colors';
import styles from './styles';


export default class Card extends Component {

    static propTypes = {
        title: PropTypes.string,
        icon: PropTypes.string,
        size: PropTypes.string,
        style: PropTypes.object,
    }

    static defaultProps = {
        size: '50%'
    };

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <View
                style={style.container}
            >

                <Icon
                    name={props.card.icon}
                    size={40}
                    color={colors.BLUE_DARK}
                />

                <Text style={style.title}>
                    {props.card.title.toUpperCase()}
                </Text>

            </View>
        )
    }
}