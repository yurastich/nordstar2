import { StyleSheet } from 'react-native';
import * as sizes from 'app/config/sizes';
import * as colors from 'app/config/colors';


export default props =>
  StyleSheet.create({

    container: StyleSheet.flatten([
      {
        backgroundColor: colors.BLUE_LIGHT,
        height: 250,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignContent: 'center',
        padding: sizes.GUTTER,
        // flexBasis: props.size
      },
      props.style && {
        ...props.style
      }
    ]),

    title: StyleSheet.flatten([
      {
        color: colors.BLUE_DARK,
        textAlign: 'center',
        paddingTop: 30,
        fontSize: sizes.FONT_SIZE,
        flexBasis: '100%'
      },

    ]),

    icon: StyleSheet.flatten([
      {

      },

    ])
  });

