import React, { Component } from 'react';

import MainLayout from 'app/layouts/MainLayout';
import Content from 'app/components/Content';


class Favorites extends Component {

    cards = [
        {
            id: 'q1',
            icon: 'address-card',
            size: '100%',
            title: 'Купить билет'
        },
        {
            id: 'q2',
            icon: 'calendar',
            title: 'Онлайн табло'
        },
        {
            id: 'q3',
            icon: 'pencil',
            title: 'Онлайн регистрация'
        },
        {
            id: 'q7',
            size: '100%',
            icon: 'motorcycle',
            title: 'Моцык'
        },
    ]

    render() {
        return (
            <Content>
                <MainLayout
                    cards={this.cards}
                />
            </Content>
        );
    }
}

export default Favorites;