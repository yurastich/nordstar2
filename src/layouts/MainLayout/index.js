import React from 'react';
import PropTypes from 'prop-types';
import {
    View
} from 'react-native';

import Card from 'app/components/Card';


import styles from './styles';



export default class MainLayout extends React.Component {

    static propTypes = {
        cards: PropTypes.array,
    }

    static defaultProps = {
        cards: []
    };

    render() {
        const { props } = this;
        const style = styles(props);

        return (
            <View style={style.container}>

                {props.cards.map(card =>
                    <View
                        key={card.id}
                        style={{
                            ...style.card,
                            flexBasis: card.size || '50%',
                        }}>
                        <Card
                            card={card}
                        />
                    </View>
                )}
            </View>
        )
    }
}