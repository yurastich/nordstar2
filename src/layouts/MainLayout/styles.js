import { StyleSheet } from 'react-native';
import * as sizes from 'app/config/sizes';

// import * as styles from 'app/config/style';
// import stylesGlobal from 'app/themes/global';

export default props =>
  StyleSheet.create({
    container: StyleSheet.flatten([
      {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginHorizontal: -sizes.GUTTER/2,
        paddingVertical: sizes.GUTTER/2
      }

    ]),

    card: StyleSheet.flatten([
      {
        padding: sizes.GUTTER/2
      }

    ]),


  });

