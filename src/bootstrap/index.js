import React, { Component } from 'react';
import {
  Scene,
  Stack,
  Router
} from 'react-native-router-flux';

import * as routes from "app/config/sceneKeys";

import NavBar from "app/components/NavBar";

import Main from 'app/views/Main';


export default class Index extends Component {

  render() {

    return (
        <Router sceneStyle={{ backgroundColor: "#fff" }}>
            <Stack key="root">
              <Scene
                key={routes.MAIN.key}
                title={routes.MAIN.title}
                component={Main}
                navBar={NavBar}
                initial
              />
            </Stack>
        </Router>
    );
  }
}